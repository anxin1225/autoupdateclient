﻿namespace AutoUpdateClient
{
    partial class AutoUpdateClient
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.label_status = new System.Windows.Forms.Label();
            this.progress_bar_status = new System.Windows.Forms.ProgressBar();
            this.label_detailed = new System.Windows.Forms.Label();
            this.button_close = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label_status
            // 
            this.label_status.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label_status.Location = new System.Drawing.Point(23, 29);
            this.label_status.Name = "label_status";
            this.label_status.Size = new System.Drawing.Size(355, 23);
            this.label_status.TabIndex = 0;
            this.label_status.Text = "正在检查更新";
            this.label_status.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // progress_bar_status
            // 
            this.progress_bar_status.Location = new System.Drawing.Point(26, 128);
            this.progress_bar_status.Name = "progress_bar_status";
            this.progress_bar_status.Size = new System.Drawing.Size(351, 23);
            this.progress_bar_status.TabIndex = 1;
            this.progress_bar_status.Visible = false;
            // 
            // label_detailed
            // 
            this.label_detailed.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label_detailed.Location = new System.Drawing.Point(23, 102);
            this.label_detailed.Name = "label_detailed";
            this.label_detailed.Size = new System.Drawing.Size(355, 23);
            this.label_detailed.TabIndex = 2;
            this.label_detailed.Text = "system.dll";
            this.label_detailed.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label_detailed.Visible = false;
            // 
            // button_close
            // 
            this.button_close.Location = new System.Drawing.Point(163, 102);
            this.button_close.Name = "button_close";
            this.button_close.Size = new System.Drawing.Size(75, 23);
            this.button_close.TabIndex = 3;
            this.button_close.Text = "关闭";
            this.button_close.UseVisualStyleBackColor = true;
            this.button_close.Visible = false;
            this.button_close.Click += new System.EventHandler(this.button_close_Click);
            // 
            // AutoUpdateClient
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(400, 180);
            this.Controls.Add(this.button_close);
            this.Controls.Add(this.label_detailed);
            this.Controls.Add(this.progress_bar_status);
            this.Controls.Add(this.label_status);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "AutoUpdateClient";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "AutoUpdateClient";
            this.Load += new System.EventHandler(this.AutoUpdateClient_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label_status;
        private System.Windows.Forms.ProgressBar progress_bar_status;
        private System.Windows.Forms.Label label_detailed;
        private System.Windows.Forms.Button button_close;
    }
}

