﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AutoUpdateClient
{
    public delegate void InvokeAction(Action action);

    public partial class AutoUpdateClient : Form
    {
        public AutoUpdateClient()
        {
            InitializeComponent();
        }

        public void RunActionInMainThread(Action action)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new InvokeAction(RunActionInMainThread), new[] { action });
            }
            else
            {
                action();
            }
        }

        /// <summary>
        /// 设置进度
        /// </summary>
        /// <param name="progress"></param>
        public void SetProgress(float progress)
        {
            RunActionInMainThread(() => { this.progress_bar_status.Value = (int)(progress * 100); });
        }

        /// <summary>
        /// 设置详细
        /// </summary>
        /// <param name="detailed"></param>
        public void SetDetailed(string detailed)
        {
            RunActionInMainThread(() => { this.label_detailed.Text = detailed; });
        }

        /// <summary>
        /// 设置状态
        /// </summary>
        /// <param name="status"></param>
        public void SetStatus(string status)
        {
            RunActionInMainThread(() => { this.label_status.Text = status; });
        }

        private void AutoUpdateClient_Load(object sender, EventArgs e)
        {
            Dictionary<UpdateDataType, Action<object>> Name2Action = new Dictionary<UpdateDataType, Action<object>>();
            Name2Action.Add(UpdateDataType.Status, obj => { SetStatus(obj.ToString()); });
            Name2Action.Add(UpdateDataType.Detailed, obj => { SetDetailed(obj.ToString()); });
            Name2Action.Add(UpdateDataType.Progress, obj =>
            {
                float f = -1;
                float.TryParse(obj.ToString(), out f);
                SetProgress(f);
            });
            Name2Action.Add(UpdateDataType.SystemMessage, obj =>
            {
                var status = obj as SystemMessage?;
                if (!status.HasValue)
                    return;

                RunActionInMainThread(() =>
                {
                    switch (status.Value)
                    {
                        case SystemMessage.UpdateError:
                            button_close.Visible = true;
                            break;

                        case SystemMessage.Finsh:
                            Close();
                            break;
                        default:
                            break;
                    }
                });
            });

            AutoUpdateHelper helper = new AutoUpdateHelper();
            helper.WebXmlUrl = "http://7xs9hw.com1.z0.glb.clouddn.com/VersionInfo.json";
            helper.ConfigXmlPath = "SynchronizeVersions.xml";
            helper.TempXmlPath = "SynchronizeVersions_Temp.xml";
            helper.FilePath = "Client";

            helper.CallBack = obj =>
            {
                if (obj is Dictionary<UpdateDataType, object>)
                {
                    var dic = obj as Dictionary<UpdateDataType, object>;
                    foreach (var item in dic)
                    {
                        if (Name2Action.ContainsKey(item.Key))
                            Name2Action[item.Key](item.Value);
                    }
                }
            };

            try
            {
                helper.Start();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button_close_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
